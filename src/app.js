const Express = require("express");
const App = Express();
const BodyParser = require("body-parser");

const Routes = require("./routes");

App.use(Routes);

module.exports = App;
