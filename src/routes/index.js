const Express = require("express");
const Routes = Express.Router();

Routes.get("/hello", (req, res) => {
  return res.status(200).json({ Message: "Hello World!" });
});

module.exports = Routes;
