const app = require("../src/app");
const superTest = require("supertest");
const requestWithSupertest = superTest(app);

describe("Hello Endpoint", () => {
  it("GET /hello should show Hello", async () => {
    const res = await requestWithSupertest.get("/hello");
    expect(res.status).toEqual(200);
  });
});
